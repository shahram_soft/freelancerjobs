﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace FreelancerJobs.Controllers.Owner
{
    public class ProjectController : Controller
    {
        public IActionResult Index(string templateId,string templateName)
        {
            ViewData["TemplateId"] = templateId;
            ViewData["TemplateName"] = templateName;

            return View();
        }
        public IActionResult PriceAndDeadLine(int templateId, string projectName,string projectDescription)
        {
            ViewData["TemplateId"] = templateId;
            ViewData["ProjectName"] = projectName;
            ViewData["ProjectDescription"] = projectDescription;
            return View();
        }
    }
}
