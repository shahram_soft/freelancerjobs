﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FreelancerJob.Logic.Repositories;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace FreelancerJobs.Controllers.Owner
{
    public class HomeOwnerController : Controller
    {
        public IConfiguration Configuration { get; set; }
        private readonly SignInManager<IdentityUser> _signInManager;
        public HomeOwnerController(IConfiguration configuration, SignInManager<IdentityUser> signInManager)
        {
            Configuration = configuration;
            _signInManager = signInManager;
        }
        public IActionResult Index()
        {
            var connectionString = Configuration.GetConnectionString("FreelancerJobsContextConnection");
            var templateRepo = new ProjectTemplateRepository(connectionString);
            var allTemplates = templateRepo.GetAllTemplates();
            ViewData["AllTemplates"] = allTemplates;
            return View();
        }

        public IActionResult GetAllProjects([DataSourceRequest] DataSourceRequest dataSource)
        {
            try
            {
                var onlineuser = HttpContext.User.Identity.Name;
                var idUser = _signInManager.UserManager.Users.Where(x => x.UserName == onlineuser).FirstOrDefault();
                var connectionString = Configuration.GetConnectionString("FreelancerJobsContextConnection");
                var projectRps=new ProjectRepository(connectionString);
                var allprojectsData = projectRps.GetAllProjectsOfUser(new Guid(idUser.Id));
                return  Json(allprojectsData.ToDataSourceResult(dataSource));
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}
