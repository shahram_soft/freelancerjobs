﻿using FreelancerJob.Logic.Logic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace FreelancerJobs.Controllers
{
    public class MigrationController : Controller
    {
        public IConfiguration Configuration { get; set; }
        public MigrationController(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public bool Index()
        {
            var migrationMng = new MigrationManager(Configuration.GetConnectionString("FreelancerJobsContextConnection"));
            var result = migrationMng.RunMigrations();
            return result;
        }
    }
}
