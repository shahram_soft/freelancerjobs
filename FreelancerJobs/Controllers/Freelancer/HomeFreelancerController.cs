﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace FreelancerJobs.Controllers.Freelancer
{
    public class HomeFreelancerController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
