﻿using FreelancerJob.Logic.Interfaces;

namespace FreelancerJob.Logic.MigrationModels
{
    public class ProjectMigration : IMigrationModel
    {
        public string GetMigration()
        {
            return @"CREATE TABLE [dbo].[projects](
	[Id] [uniqueidentifier] NOT NULL,
	[ProjectName] [nvarchar](400) NULL,
	[Description] [nvarchar](max) NULL,
	[PriceRange] [nvarchar](100) NULL,
	[DeadLine] [datetime] NULL,
	[ownerId] [uniqueidentifier] NULL,
	[TemplateId] [uniqueidentifier] NULL,
 CONSTRAINT [PK_projects] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]";
        }
    }
}
