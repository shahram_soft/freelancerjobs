﻿using FreelancerJob.Logic.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace FreelancerJob.Logic.MigrationModels
{
    public class ProjectTemplateMigration : IMigrationModel
    {
        public string GetMigration()
        {
			return @"CREATE TABLE [dbo].[ProjectTemplate](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](200) NULL,
	[Pic] [varbinary](max) NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_ProjectTemplate] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]";
		}
    }
}
