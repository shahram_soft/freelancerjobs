﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FreelancerJob.Logic.ViewModels
{
 public   class ProjectViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string PriceRange { get; set; }
        public DateTime  DeadLine { get; set; }
        public string StringDate { get; set; }

    }
}
