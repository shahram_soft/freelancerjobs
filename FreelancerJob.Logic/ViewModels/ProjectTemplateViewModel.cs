﻿using System;

namespace FreelancerJob.Logic.ViewModels
{
    public class ProjectTemplateViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Pic { get; set; }
        public byte[] GeneralPic { get; set; }
    }
}
