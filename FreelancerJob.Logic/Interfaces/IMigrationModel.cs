﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FreelancerJob.Logic.Interfaces
{
    interface IMigrationModel
    {
        string GetMigration();
    }
}
