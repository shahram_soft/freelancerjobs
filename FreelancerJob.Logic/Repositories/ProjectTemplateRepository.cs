﻿using FreelancerJob.Logic.Logic;
using FreelancerJob.Logic.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;

namespace FreelancerJob.Logic.Repositories
{
  public  class ProjectTemplateRepository
    {
        public string ConnectionString { get; set; }
        public SqlQueryRunner SqlQueryRunner { get; set; }
        public ProjectTemplateRepository(string connectionString)
        {
            ConnectionString = connectionString;
            SqlQueryRunner = new SqlQueryRunner(connectionString);
        }
        public List<ProjectTemplateViewModel> GetAllTemplates()
        {
            try
            {
                var lstallPrjectsOfUser = new List<ProjectTemplateViewModel>();
              
                var query = "select * from ProjectTemplate";
                var result = SqlQueryRunner.ExecuteDataSet(query);
                if (result.Tables.Count > 0)
                {
                    var firstTableData = result.Tables[0];
                    foreach (DataRow dataRow in firstTableData.Rows)
                    {
                        var templateModel = new ProjectTemplateViewModel
                        {
                            Id = new Guid(dataRow["Id"].ToString()),
                            Name = dataRow["Name"].ToString(),
                            Description = dataRow["Description"].ToString()
                        };
                        lstallPrjectsOfUser.Add(templateModel);
                    }
                }

                return lstallPrjectsOfUser;

            }
            catch (Exception ex)
            {

                return new List<ProjectTemplateViewModel>();
            }
        }
    }
}
