﻿using FreelancerJob.Logic.Logic;
using FreelancerJob.Logic.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace FreelancerJob.Logic.Repositories
{
  public  class ProjectRepository
    {
        public string ConnectionString { get; set; }
        public SqlQueryRunner SqlQueryRunner { get; set; }
        public ProjectRepository(string connectionString)
        {
            ConnectionString = connectionString;
            SqlQueryRunner = new SqlQueryRunner(connectionString);
        }

        public List<ProjectViewModel> GetAllProjectsOfUser(Guid userId)
        {
            try
            {
                var lstallPrjectsOfUser=new List<ProjectViewModel>();
                //var projectDatafirst = new ProjectViewModel
                //{
                //    Id = new Guid(),
                //    Name = "Test Project",
                //    Description = "This project Is About",
                //    PriceRange = "2$-5$",
                //    DeadLine = DateTime.Now
                //};
                //projectDatafirst.StringDate = projectDatafirst.DeadLine.Year + "/" + projectDatafirst.DeadLine.Month + "/" +
                //                              projectDatafirst.DeadLine.Day;
                //lstallPrjectsOfUser.Add(projectDatafirst);
                var query = "select * from projects where ownerId='" + userId+"'";
                var result = SqlQueryRunner.ExecuteDataSet(query);
                if (result.Tables.Count > 0)
                {
                    var firstTableData = result.Tables[0];
                    foreach (DataRow dataRow in firstTableData.Rows)
                    {
                        var projectData=new ProjectViewModel
                        {
                            Id = new Guid(dataRow["Id"].ToString() ?? string.Empty),
                            Name = dataRow["ProjectName"].ToString(),
                            Description = dataRow["Description"].ToString(),
                            PriceRange = dataRow["PriceRange"].ToString(),
                            DeadLine = DateTime.Parse(dataRow["DeadLine"].ToString() ?? string.Empty)
                        };
                        projectData.StringDate = projectData.DeadLine.Year + "/" + projectData.DeadLine.Month + "/" +
                                                 projectData.DeadLine.Day;
                        lstallPrjectsOfUser.Add(projectData);
                    }
                }

                return lstallPrjectsOfUser;
            }
            catch (Exception e)
            {
                return new List<ProjectViewModel>();
            }
        }
    }
}
