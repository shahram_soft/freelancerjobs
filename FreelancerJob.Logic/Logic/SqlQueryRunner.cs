﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace FreelancerJob.Logic.Logic
{
 public   class SqlQueryRunner
    {
        public string ConnectionString { get; set; }
        public SqlQueryRunner(string connectionString)
        {
            ConnectionString = connectionString;
        }

        public DataSet ExecuteDataSet(string query)
        { 
                DataSet OdsGeneralEntity = new DataSet();
                SqlConnection conn = new SqlConnection(ConnectionString);
                SqlCommand cmd = new SqlCommand(query, conn);
               
            try
            {
                conn.Open();

                // create data adapter
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                // this will query your database and return the result to your datatable
                da.Fill(OdsGeneralEntity);
                conn.Close();
                da.Dispose();
                //==================
                // CtrlsDataMask.FillDataset(CtrlsDataMask.SqlConnectionType.GenCnn, CommandType.Text, textcommand, OdsGeneralEntity, new string[] { "TblAllObjects" });
                return OdsGeneralEntity;
            }
            catch (Exception e)
            {
                conn.Close();
                return new DataSet();
            }
        }
        public bool ExecuteOnly(string query)
        {
                SqlConnection conn = new SqlConnection(ConnectionString);
                SqlCommand cmd = new SqlCommand(query, conn);
                
            try
            {
                conn.Open();

                // create data adapter
                cmd.ExecuteNonQuery();
                conn.Close();
               
                //==================
                // CtrlsDataMask.FillDataset(CtrlsDataMask.SqlConnectionType.GenCnn, CommandType.Text, textcommand, OdsGeneralEntity, new string[] { "TblAllObjects" });
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}
