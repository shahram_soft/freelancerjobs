﻿using FreelancerJob.Logic.Interfaces;
using System;
using System.Linq;
using System.Reflection;

namespace FreelancerJob.Logic.Logic
{
    public  class MigrationManager
    {
        public string ConnectionString { get; set; }
        public MigrationManager(string connectionString)
        {
            ConnectionString = connectionString;

        }
        public bool RunMigrations()
        {
            try
            {
                var queryRunnerMng = new SqlQueryRunner(ConnectionString);
                var type = typeof(IMigrationModel);
                var types = AppDomain.CurrentDomain.GetAssemblies()
                    .SelectMany(s => s.GetTypes())
                    .Where(p => type.IsAssignableFrom(p));
                foreach (var item in types)
                {
                    Object obj = Activator.CreateInstance(item);
                    // Retrieve the method you are looking for
                    MethodInfo methodInfo = type.GetMethod("GetMigration");
                    // Invoke the method on the instance we created above
                   var result= methodInfo.Invoke(obj, null).ToString();
                    queryRunnerMng.ExecuteOnly(result);
                }
                return true;
            }
            catch (Exception)
            {
                return false;


            }
        }
    }
}
